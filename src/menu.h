#ifndef MENU_H
#define MENU_H

#include "Arduino.h"
#include "output.h"

namespace Menu {


/*
   Settings
*/
#define MENU_STACK_SIZE    5

#define MENU_ACTION_END   -1
#define MENU_ACTION_BACK  -2


/*
   Structure
*/
struct MenuEntry {
  private:
    MenuEntry(const MenuEntry&) = delete;

  public:
    int id = 0;
    const __FlashStringHelper *name;

    int childCount = 0;
    MenuEntry *children  = NULL;

    MenuEntry(const __FlashStringHelper *name, int id) : name(name), id(id) {};
    MenuEntry(const __FlashStringHelper *name, MenuEntry *children, int childCount) : name(name), children(children), childCount(childCount) {};
};


/*
   Menu Engine
*/
class MenuEngine {
  private:
    Output *output;
    MenuEntry *root;
    MenuEntry *stack[MENU_STACK_SIZE]; // max stack size
    int selection[MENU_STACK_SIZE];
    int stackPosition = 0;

    boolean hasChange = true;
    boolean shouldClose = false;
    char* (*dynString)(int id) = NULL;

  public:
    MenuEngine(Output *output, MenuEntry *root) : output(output), root(root) {
      stack[0] = root;
      selection[0] = 0;
    };
    MenuEngine(Output *output, MenuEntry *root, char* (*dynString)(int id)) : output(output), root(root), dynString(dynString) {
      stack[0] = root;
      selection[0] = 0;
    };

    void print();
    void print(bool force);
    void setChanged();

    void scroll(int dir);
    void select();
    void back();
    void exit();

    int getSelection();
};

}
#endif
