#include "output.h"

/*
   Output
*/
void Output::setup() {
  pinMode(pinLcdD4, OUTPUT);
  pinMode(pinLcdD5, OUTPUT);
  pinMode(pinLcdD6, OUTPUT);
  pinMode(pinLcdD7, OUTPUT);
  pinMode(pinLcdEnable, OUTPUT);
  pinMode(pinLcdReset, OUTPUT);

  pinMode(pinLed1, OUTPUT);
  pinMode(pinLed2, OUTPUT);
  pinMode(pinLed3, OUTPUT);
  pinMode(pinLed4, OUTPUT);

  begin(16, 2);
  noCursor();
  noBlink();
}

void Output::write() {
  setCursor(0, 0);
  print("Hello World!");
}

void Output::signalPosition(int pos) {
  setCursor(pos, 1);
}

