#include "input.h"

/*
   Debounced Button
*/
void DebouncedButton::read() {
  int reading = digitalRead(pin);

  if (reading != lastButtonState) {
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    if (reading != buttonState) {
      buttonState = reading;
    }
  }

  lastButtonState = reading;
}

ButtonState DebouncedButton::getState() {
  switch (extButtonState) {
    case UP:
      if (buttonState == LOW)
        extButtonState = GOING_DOWN;
      break;
    case GOING_DOWN:
      extButtonState = DOWN;
      break;
    case DOWN:
      if (buttonState == HIGH)
        extButtonState = GOING_UP;
      break;
    case GOING_UP:
      extButtonState = UP;
      break;
  }
  return extButtonState;
}


/*
   Input
*/
void Input::setup() {
  pinMode(pinEncA, INPUT_PULLUP);
  pinMode(pinEncB, INPUT_PULLUP);

  pinMode(pinButX, INPUT_PULLUP);
  pinMode(pinButLeft, INPUT_PULLUP);
  pinMode(pinButRight, INPUT_PULLUP);

  buttonX = new DebouncedButton(pinButX);
  buttonLeft = new DebouncedButton(pinButLeft);
  buttonRight = new DebouncedButton(pinButRight);
}

void Input::scan() {
  readEncoder();
  buttonX->read();
  buttonLeft->read();
  buttonRight->read();
}

byte Input::getEncState() {
  return (digitalRead(pinEncA) ? 1 : 0) + (digitalRead(pinEncB) ? 2 : 0);
}

void Input::readEncoder() {
  byte newState = 0;

  encSpeed = 0;
  newState = getEncState();

  if (newState == prevEncState) {
    return;
  }

  if ((prevEncState == 0 && newState == 2) ||
      (prevEncState == 2 && newState == 3) ||
      (prevEncState == 3 && newState == 1) ||
      (prevEncState == 1 && newState == 0)) {
    encSpeed--;
  }
  if ((prevEncState == 0 && newState == 1) ||
      (prevEncState == 1 && newState == 3) ||
      (prevEncState == 3 && newState == 2) ||
      (prevEncState == 2 && newState == 0)) {
    encSpeed++;
  }

  prevEncState = newState;
  encChange += encSpeed;
}

ButtonState Input::getButtonXState() {
  return buttonX->getState();
}
ButtonState Input::getButtonLeftState() {
  return buttonLeft->getState();
}
ButtonState Input::getButtonRightState() {
  return buttonRight->getState();
}

int Input::getEncoderChange() {
  int result = encChange;
  encChange = 0;
  return result;
}

