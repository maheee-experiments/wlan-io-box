#include "setup.h"
#include "config.h"
#include "input.h"
#include "output.h"
#include "menu.h"

using namespace Menu;

/*
   Initialize Modules
*/
Input input(PIN_ENC_A, PIN_ENC_B, PIN_BUTTON_X, PIN_BUTTON_L, PIN_BUTTON_R);
Output output(PIN_LCD_D4, PIN_LCD_D5, PIN_LCD_D6, PIN_LCD_D7, PIN_LCD_ENABLE, PIN_LCD_RESET, PIN_LED_1, PIN_LED_2, PIN_LED_3, PIN_LED_4);


/*
   Setup Timer
*/
hw_timer_t *inputTimer;

void IRAM_ATTR onInputTimer() {
  input.scan();
}


/*
   Setup
*/
void setup() {
  Serial.begin(38400);
  Serial.flush();

  input.setup();
  output.setup();

  inputTimer = timerBegin(0, 80, true); // prescaler 80
  timerAttachInterrupt(inputTimer, &onInputTimer, true);
  timerAlarmWrite(inputTimer, 1000, true); // counter 1000
  timerAlarmEnable(inputTimer); // 80 MHz / (80*1000) == 1000 Hz

  output.write();
}


/*
   Program Loop
*/
ButtonState buttonXState;
ButtonState buttonLeftState;
ButtonState buttonRightState;

int encChange = 0;

bool menuOpen = false;

void loop() {
  buttonXState = input.getButtonXState();
  buttonLeftState = input.getButtonLeftState();
  buttonRightState = input.getButtonRightState();

  encChange += input.getEncoderChange();

  if (menuOpen) {
    menuOpen = handleMenu(
                 buttonXState == GOING_DOWN,
                 buttonLeftState == GOING_DOWN,
                 buttonRightState == GOING_DOWN,
                 encChange / 2);
    encChange %= 2;
  } else {
    if (buttonXState == GOING_DOWN) {
      menuOpen = true;
    }
    encChange = 0;
  }

  digitalWrite(PIN_LED_1, buttonXState == DOWN);
  digitalWrite(PIN_LED_2, buttonLeftState == DOWN);
  digitalWrite(PIN_LED_3, buttonRightState == DOWN);
  digitalWrite(PIN_LED_4, millis() / 1000 % 2);

  delay(10);
}

