#ifndef OUTPUT_H
#define OUTPUT_H

#include <Arduino.h>
#include <LiquidCrystal.h>


/*
   Output
*/
class Output : public LiquidCrystal {
  private:
    int pinLcdD4, pinLcdD5, pinLcdD6, pinLcdD7;
    int pinLcdEnable, pinLcdReset;
    int pinLed1, pinLed2, pinLed3, pinLed4;

  public:
    Output(int pinLcdD4, int pinLcdD5, int pinLcdD6, int pinLcdD7, int pinLcdEnable, int pinLcdReset, int pinLed1, int pinLed2, int pinLed3, int pinLed4):
      LiquidCrystal(pinLcdReset, pinLcdEnable, pinLcdD4, pinLcdD5, pinLcdD6, pinLcdD7),
      pinLcdD4(pinLcdD4), pinLcdD5(pinLcdD5), pinLcdD6(pinLcdD6), pinLcdD7(pinLcdD7),
      pinLcdEnable(pinLcdEnable), pinLcdReset(pinLcdReset),
      pinLed1(pinLed1), pinLed2(pinLed2), pinLed3(pinLed3), pinLed4(pinLed4) {
    };

    void setup();
    void write();

    void signalPosition(int pos);
};

#endif
