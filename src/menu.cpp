#include "menu.h"

namespace Menu {

/*
   Menu Engine
*/
void MenuEngine::print() {
  print(false);
}

void MenuEngine::print(bool force) {
  if (hasChange || force) {
    if (stack[stackPosition]->childCount >= 0) {
      output->clear();
      output->print(stack[stackPosition]->children[selection[stackPosition]].name);
      output->setCursor(0, 1);
      if (dynString) {
        output->print(dynString(stack[stackPosition]->children[selection[stackPosition]].id));
      }
    }

    hasChange = false;
  }
}

void MenuEngine::setChanged() {
  hasChange = true;
}

void MenuEngine::scroll(int dir) {
  if (dir == 0) {
    return;
  }
  if (stack[stackPosition]->childCount == 0) {
    return;
  }

  selection[stackPosition] += dir;
  if (selection[stackPosition] < 0) {
    selection[stackPosition] = 0;
  }
  if (selection[stackPosition] >= stack[stackPosition]->childCount) {
    selection[stackPosition] = stack[stackPosition]->childCount - 1;
  }
  hasChange = true;
}

void MenuEngine::select() {
  if (stack[stackPosition]->childCount == 0) {
    return;
  }

  if (stackPosition < MENU_STACK_SIZE - 1) {
    stack[stackPosition + 1] = &(stack[stackPosition]->children[selection[stackPosition]]);
    stackPosition++;
    selection[stackPosition] = 0;
  }
  hasChange = true;
}

void MenuEngine::back() {
  if (stackPosition > 0) {
    stackPosition--;
  } else {
    selection[stackPosition] = 0;
    shouldClose = true;
  }
  hasChange = true;
}

void MenuEngine::exit() {
  stackPosition = 0;
  selection[0] = 0;
  hasChange = true;
  shouldClose = true;
}

int MenuEngine::getSelection() {
  if (shouldClose) {
    shouldClose = false;
    hasChange = true;
    return MENU_ACTION_END;
  } else if (stack[stackPosition]->childCount == 0) {
    return stack[stackPosition]->id;
  } else {
    return 0;
  }
}

}

