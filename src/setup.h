#ifndef SETUP_H
#define SETUP_H


#define GPIO_00   0 // ADC_11 // TOUCH_1 // hardware button on board
#define GPIO_01   1 // 
#define GPIO_02   2 // ADC_12 // TOUCH_2
#define GPIO_03   3 // 
#define GPIO_04   4 // ADC_10 // TOUCH_0
#define GPIO_05   5 // 
// 6 - 11 => FLASH
#define GPIO_12  12 // ADC_15 // TOUCH_5
#define GPIO_13  13 // ADC_14 // TOUCH_4
#define GPIO_14  14 // ADC_16 // TOUCH_6
#define GPIO_15  15 // ADC_13 // TOUCH_3
#define GPIO_16  16 // 
#define GPIO_17  17 // 
#define GPIO_18  18 // 
#define GPIO_19  19 // 
// 20 => missing
#define GPIO_21  21 // 
#define GPIO_22  22 // 
#define GPIO_23  23 // 
// 24 => missing
#define GPIO_25  25 // ADC_18
#define GPIO_26  26 // ADC_19
#define GPIO_27  27 // ADC_17 // TOUCH_7
// 28 - 31 => missing
#define GPIO_32  32 // ADC_04 // TOUCH_9
#define GPIO_33  33 // ADC_05 // TOUCH_8
#define GPIO_34  34 // ADC_06 // input only, no pullup/pulldown
#define GPIO_35  35 // ADC_07 // input only, no pullup/pulldown
#define GPIO_36  36 // ADC_00 // input only, no pullup/pulldown
#define GPIO_39  39 // ADC_03 // input only, no pullup/pulldown


#endif
