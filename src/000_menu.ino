using namespace Menu;

MenuEntry menuSettings[] = {
  { F("< back"), MENU_ACTION_BACK },
  { F("- Enable alarm"),   52 },
  { F("- Enable sensor"),  53 },
};

MenuEntry menuMain[] = {
  { F("< back"), MENU_ACTION_BACK },
  { F("- Start Timer"),     2 },
  { F("- Explode now"),     3 },
  { F("- Disarm"),          4 },
  { F("> Settings"), menuSettings, 3}
};

MenuEntry menuRoot( F(""), menuMain, 5 );

MenuEngine menu(&output, &menuRoot, &menuDescription);

/*
   Menu Logic
*/
bool handleMenu(bool selectButtonDown, bool backButtonDown, bool exitButtonDown, int scroll) {
  menu.scroll(scroll);
  menu.print();

  if (selectButtonDown) {
    menu.select();
  }
  if (backButtonDown) {
    menu.back();
  }
  if (exitButtonDown) {
    menu.exit();
  }

  int selection = menu.getSelection();

  switch (selection) {
    case MENU_ACTION_END:
      output.clear();
      return false;
    case MENU_ACTION_BACK:
      menu.back();
      menu.back();
      return true;
    case 0:
      return true;
    default:
      return runMenuAction(selection);
  }
}

/*
   Menu Actions
*/
boolean runMenuAction(int id) {
  switch (id) {
    case 2:
    case 3:
    case 4:
    case 52:
    case 53:
      Serial.println(id);
      // do something
      menu.back();
      break;
  }
  return true;
}


/*
   Dynamic Descriptions for Menu
*/
char* menuDescription(int id) {
  switch (id) {
    case 2:
      return "  A";
    case 3:
      return "  B";
    case 4:
      return "  C";
    case 52:
      return "  D";
    case 53:
      return "  E";
  }
  return "";
}

