#ifndef INPUT_H
#define INPUT_H

#include <Arduino.h>


/*
   Debounced Button
*/
enum ButtonState {
  UP,
  GOING_DOWN,
  DOWN,
  GOING_UP
};

class DebouncedButton {
  private:
    int pin;
    int buttonState = HIGH;
    int lastButtonState = HIGH;
    unsigned long lastDebounceTime = 0;
    unsigned long debounceDelay = 50;
    ButtonState extButtonState = DOWN;

  public:
    DebouncedButton(int pin): pin(pin) {};
    void read();
    ButtonState getState();
};


/*
   Input
*/
class Input {
  private:
    byte getEncState();
    void readEncoder();
    byte prevEncState = 0;
    int encSpeed = 0;
    int encChange = 0;
    DebouncedButton *buttonX, *buttonLeft, *buttonRight;

  public:
    int pinEncA;
    int pinEncB;
    int pinButX;
    int pinButLeft;
    int pinButRight;

    Input(int pinEncA, int pinEncB, int pinButX, int pinButLeft, int pinButRight):
      pinEncA(pinEncA), pinEncB(pinEncB), pinButX(pinButX), pinButLeft(pinButLeft), pinButRight(pinButRight) {
    };

    void setup();
    void scan();

    ButtonState getButtonXState();
    ButtonState getButtonLeftState();
    ButtonState getButtonRightState();

    int getEncoderChange();
};

#endif
